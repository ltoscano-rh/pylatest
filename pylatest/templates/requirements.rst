TODO Requirements
*****************

This is a *Requirements Document* of TODO.

This file is written in rst file format, if you are not familiar with it,
check:

* https://en.wikipedia.org/wiki/ReStructuredText
* http://docutils.sourceforge.net/docs/user/rst/quickref.html
* http://sphinx-doc.org/rest.html

In addition to that, you can use the following syntax to reference:

* redhat bugzilla: see :RHBZ:`439858` (it will create proper link)
* pylatest test cases: see :doc:`/pylatest_testcase_id`

.. requirement:: TODO-01
    :priority: TODO:write-a-priority-here

    TODO: write a description of 1st requirement, make sure you maintain 4
    space indentation in the whole text section which describes this step.

    The only mandatory information is the list of test cases related to this
    requirement (if such test cases exist):

    * :doc:`/testcase_something`
    * :doc:`/testcase_something_else`
    * :doc:`/testcase_yet_another_one`

.. requirement:: TODO-02
    :priority: low

    TODO
