.. _pysource:

===============================================
 Extracting Test Cases from Python Source Code
===============================================

Extraction of test case rst files from python source code via ``py2pylatest``
command line tool requires now deprecated directives :rst:dir:`test_step` and
:rst:dir:`test_result`.
